package INF102.lab2.list;

import java.lang.reflect.Array;
import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n = 0;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@SuppressWarnings("unchecked")
	@Override
	public T get(int index) {
		if (index < 0 || index >= n || isEmpty()) //O(1)
			throw new IndexOutOfBoundsException(); //O(1)
		return (T) elements[index]; //O(1)
	} //result O(1)
	
	@Override
	public void add(int index, T element) {
		if (index < 0 || index > n) //o(1)
			throw new IndexOutOfBoundsException(); //o(1)
		n++;
		if(n == elements.length){
			Object newArray[] = Arrays.copyOf(elements, (n)*2);
			elements = newArray;
		}

		T overwritten = null;
		for(int i = index; i < n; i++){
			T temp = get(i);
			elements[i] = overwritten;
			overwritten = temp;
		}
		elements[index] = element;
	} //result O(n)
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}